

function aa_secondsToTimecode (ss, style) {
    style = timecode_styles[style || 'html5'];
    var h = Math.floor(ss/3600),
        m,
        s,
        fract,
        ret;
    ss -= (h * 3600);
    m = Math.floor(ss / 60);
    ss -= (m * 60);
    s = Math.floor(ss);
    fract = style.delimiter + (""+(ss - s)).substr(2, 3);
    while (fract.length < 4) { fract += "0"; } // PAD TO ALWAYS BE THREE DIGITS LONG (eg .040)
    ret = (h || style.requireHours ? ((((h<10)?"0":"")+h)+":") : '')
    ret += (((m<10)?"0":"")+m)+":"+(((s<10)?"0":"")+s)+fract;
    return ret;
}

timecode_styles = {
    'srt': {
        requireHours: true,
        delimiter: ','
    },
    'html5': {
        requireHours: false,
        delimiter: '.'
    }
}


function aa_timecodeToSeconds (tc) {
    var tcpat = /^(?:(\d\d):)?(\d\d):(\d\d(?:[\.,](?:\d{1,3}))?)$/,
        groups = tcpat.exec(tc);
    if (groups != null) {
        var h = groups[1] !== undefined ? parseInt(groups[1]) : 0,
            m = parseInt(groups[2]),
            s = parseFloat(groups[3].replace(/,/, "."));
        return (h * 3600) + (m * 60) + s;
    }
}

function parse_fragment (fragment) {
	var parts = fragment.split('&'),
		part, m;
	for (var i=0, l=parts.length; i<l; i++) {
		part = parts[i];
		m = part.match(/^t=(.+?)(?:,(.+))?$/);
		if (m !== null) {
			this.timeStart = m[1];
			if (m[2]) {
				this.timeEend = m[2];
			}
		}
		m = part.match(/^line=(.+?)(?:,(.+))?$/);
		if (m !== null) {
			this.lineStart = parseInt(m[1]);
			if (m[2]) {
				this.lineEnd = parseInt(m[2]);
			}
		}
	}
}

function parse_href (href) {
	var that = {
			href: href
		},
		hashpos = href.indexOf('#'),
		base = href,
		fragment = null;
	if (hashpos >= 0) {
		base = href.substr(0, hashpos);
		fragment = href.substr(hashpos+1);
		parse_fragment.call(that, fragment);
	}
	that['base' ] = base;
	that['nofrag' ] = base;
	that['basehref' ] = base;
	that['fragment'] = fragment;
	var lsp = base.lastIndexOf("/");
	that['basename'] = (lsp !== -1) ? base.substr(lsp+1) : base; 

	// if (mf.hash && mf.hash.t && mf.hash.t.length >= 1) {
	// 	that['start'] = mf.hash.t[0].startNormalized;
	// 	that['end'] = mf.hash.t[0].endNormalized;
	// }

	return that;
}

function editor (elt, ace) {
    ace = ace || window.ace;

    var that = {},
    	aceeditor,
    	sessionsByHref = {},
	    modelist = ace.require("ace/ext/modelist"),
	    TokenClicks = ace.require('kitchen-sink/token_clicks').TokenClicks,
        tokenclicks,
        current_href,
        highlightLineStart,
        highlightLineEnd;

    that.get_session = function (href) {
        return sessionsByHref[href];
    }

    var root = d3.select(elt)
            .attr("class", "editor"),
        body = root.append("div")
            .attr("class", "editorbody"),
        foot = root.append("div")
            .attr("class", "editorfoot");

    body.style({position: "absolute", left: 0, top: 0, width: "100%", bottom: "32px"});
    foot.style({position: "absolute", left:  0, bottom: 0, width: "100%", height: "32px", "z-index": 10});

    aceeditor = ace.edit(body[0][0]);

    function init () {
        aceeditor.setOption("wrap", true);
        aceeditor.renderer.setShowPrintMargin(false);
    }
    init();

    tokenclicks = new TokenClicks(aceeditor, {
        tokenChange: function (token) {
            // console.log("token", token.type)
            if (token != null && (
                (token.type == "heading.timecode.start") ||
                (token.type == "heading.timecode.end") )) {
                return true;
            }
            if (token != null && 
                token.type == "markup.underline") {
                return true;
            }
        }
    });
    tokenclicks.on("tokenclick", function (token) {
        // console.log("tokenclick", token);
        if ((token.type == "heading.timecode.start") || (token.type == "heading.timecode.end")) {
            var t = aa_timecodeToSeconds(token.value),
                tc;
            if (t) {
                // console.log("timecode click", t);
                window.postMessage({
                    msg: "fragmentclick",
                    origin: window.name,
                    startNormalized: t,
                    start: aa_secondsToTimecode(t)
                }, "*");
                // var href = current_href.replace(/\.srt$/, '');
                // $(editor.elt).trigger("fragmentclick", { href: href+"#t="+aa.secondsToTimecode(t) });
            } else {
                console.log("bad timecode");
            }
        } // else if (token.type == "markup.underline") { }
    });
    
    // a bit of a brutal solution but...
    $(document).bind("resize", function (e) { aceeditor.resize(); });

    // MODE SELECTOR
    var modeselector = foot
        .append("select")
        .attr("class", "editormode");

    modeselector
        .on("change", function () {
            // console.log("change", this, this.value);
            aceeditor.getSession().setMode(this.value);
        })
        .selectAll("option")
        .data(modelist.modes)
        .enter()
        .append("option")
        .attr("value", function (d) { return d.mode; })
        .text(function (d) { return d.caption; });

    function do_save () {
        var text = aceeditor.getValue();
        status.innerHTML = "saving...";
        // console.log(editHref, text);
        $.ajax({
            url: "/cookbook/cgi-bin/saveas.cgi",
            data: {
                path: current_href,
                data: text
            },
            method: 'post',
            dataType: 'json',
            success: function (response) {
                // console.log("saved", response);
                status.innerHTML = "saved " + new Date();
            },
            error: function (response) {
              console.log("error", response);
            }
        });
    }

    var save = foot
        .append("button")
        .text("save")
        .on("click", do_save);

    var status = document.createElement("span");
    foot[0][0].appendChild(status);

    var theme_select = document.getElementById("theme");
    foot[0][0].appendChild(theme_select);
    theme.addEventListener("change", function () {
        console.log("theme", this.value);
        aceeditor.setTheme(this.value)
    });


    function highlight(s, e) {
        var session = aceeditor.getSession();
        if (highlightLineStart) {
            for (var i=(highlightLineStart-1); i<=(highlightLineEnd-1); i++) {
                session.removeGutterDecoration(i, 'ace_gutter_active_annotation');
            }
        }
        highlightLineStart = s;
        highlightLineEnd = e;
        if (highlightLineStart) {
            for (var i=(highlightLineStart-1); i<=(highlightLineEnd-1); i++) {
                session.addGutterDecoration(i, 'ace_gutter_active_annotation');
            }
            aceeditor.scrollToLine(highlightLineStart, true, true);
        }
    }

    that.href = function (href, done) {
        if (arguments.length == 0) {
            var ret = current_href;
            if (highlightLineStart) {
                ret = lineRangeHref(current_href, highlightLineStart, highlightLineEnd);
            }
            return ret;
        }
        // console.log("href", href, done);
        href = parse_href(href);
        // console.log("parsed", href);
        var session = sessionsByHref[href.nofrag];
        current_href = href.nofrag;

        if (session == "loading") {
            return false;
        }

        if (session != undefined) {
            if (done) {
                window.setTimeout(function () {
                    done.call(session);
                }, 0);
            }
            aceeditor.setSession(session.acesession);
            // deal with eventual changed fragment
            if (href.lineStart) {
                highlight(href.lineStart, href.lineEnd);
                $(editor.elt).trigger("fragmentupdate", {editor: editor});
            }

            return true;
        }

        sessionsByHref[href.nofrag] = "loading";
        $.ajax({
            url: href.nofrag,
            data: { f: (new Date()).getTime() },
            success: function (data) {
                // console.log("got data", data);
                var mode = modelist.getModeForPath(href.nofrag).mode || "ace/mode/text",
                    session = { href: href.nofrag, editor: editor };
                
                session.acesession = ace.createEditSession(data, mode);
                // index(href.nofrag, data);
                modeselector[0][0].value = mode;
                aceeditor.setSession(session.acesession);
                // editor.setOption("showLineNumbers", false);
                aceeditor.setHighlightActiveLine(false);
                session.acesession.setUseWrapMode(true);
                sessionsByHref[href.nofrag] = session;
                if (done) {
                    window.setTimeout(function () {
                        done.call(session);
                    }, 0);
                }
            },
            error: function (code) {
                console.log("aceeditor: error loading", href.nofrag, code);
            }
        });
    }

    // ??? 
    var observed_href = null;
    $(document).on("fragmentupdate", function (e, data) {
        if ((e.target !== editor.elt) && data.editor) {
            observed_href = data.editor.href();
            // console.log("ace.fragmentupdate", e.target, observed_href);
        }
    });

    /*
    that.newSession = function () {
        var session = ace.createEditSession("", "ace/mode/srt-md");
        aceeditor.setSession(session);
    }
    */

    function bind_keys (e) {
        e.commands.addCommand({
            name: 'pasteTimecode',
            bindKey: {win: 'ctrl-shift-down',  mac: 'command-shift-down'},
            exec: function () {
                var mode = aceeditor.getSession().getMode().$id,
                    link,
                    t;
                console.log("paste", window, window.parent);
                if (last_fragment && last_fragment.hash.t.length) {
                    t = last_fragment.hash.t[0];
                    if (mode == "ace/mode/srtmd") {
                        link = aa_secondsToTimecode(t.startNormalized)+" -->\n";
                    } else if (mode == "ace/mode/markdown") {
                        link = "["+last_fragment.value+"]("+last_fragment.value+")";
                    } else {
                        link = last_fragment.value;
                    }
                    aceeditor.insert(link);
                }
            },
            readOnly: false
        });
        e.commands.addCommand({
            name: 'save',
            bindKey: {win: 'ctrl-s',  mac: 'command-s'},
            exec: function () {
                do_save();
            },
            readOnly: false
        });
        e.commands.addCommand({
            name: 'toggleMedia',
            bindKey: {win: 'ctrl-shift-up',  mac: 'command-shift-up'},
            exec: function () {
                window.postMessage({msg: "cc_toggle", origin: window.name}, "*");
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        e.commands.addCommand({
            name: 'jumpMediaBack',
            bindKey: {win: 'ctrl-shift-left',  mac: 'command-shift-left'},
            exec: function () {
                window.postMessage({msg: "cc_left", origin: window.name}, "*");
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        e.commands.addCommand({
            name: 'jumpMediaForward',
            bindKey: {win: 'ctrl-shift-right',  mac: 'command-shift-right'},
            exec: function () {
                window.postMessage({msg: "cc_right", origin: window.name}, "*");
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        /*
        e.commands.addCommand({
            name: 'newSession',
            bindKey: {win: 'ctrl-n',  mac: 'command-n'},
            exec: function () {
                // console.log("NEW");
                editor.newSession();
            },
            readOnly: true // false if this command should not apply in readOnly mode
        });
        */
    }
    bind_keys(aceeditor);

    return that;
}