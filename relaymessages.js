(function () {

// http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function relaymessages () {
	// Array.prototype.slice.call(
	var uuid = guid(),
		attr = "data-relaymessages-"+uuid;
	console.log("relaymessages", uuid)

	function relay_iframe_messages_to_others (f) {
		// console.log("WATCHING", f, f.contentWindow);
		f.contentWindow.addEventListener("message", function (e) {
			// onsole.log("RECV", e, "source", e.source, "cwindow", f.contentWindow);
			// only propagate messages originating from the frame itself (and thus not re-propagated messages)
			var iframes = document.querySelectorAll("iframe");
			if (e.source == f.contentWindow) {
				// console.log("RECV2", e, iframes.length);
				for (var i=0, l=iframes.length; i<l; i++) {
					var f2 = iframes[i];
					// don't propate to yourself
					// if (e.source !== f2.contentWindow) { // should be the same as below
					if (f !== f2) {
						// console.log("relaying message from", f, "to", f2);
						f2.contentWindow.postMessage(e.data, "*");
					} else {
						console.log("skipping self")
					}
				}
			}
		});
	}

	window.setInterval(function () {
		var iframes = document.querySelectorAll("iframe");
		for (var i=0,l=iframes.length; i<l; i++) {
			if (!iframes[i].getAttribute(attr)) {
				iframes[i].setAttribute(attr, "1");
				console.log("relaying messages from new iframe", iframes[i]);
				relay_iframe_messages_to_others(iframes[i]);
			}
		}
		// console.log("relaymessages", iframes.length);
	}, 2500);
	return;
	// event bridge
}

window.relaymessages = relaymessages;

document.addEventListener("DOMContentLoaded", relaymessages);

})();

